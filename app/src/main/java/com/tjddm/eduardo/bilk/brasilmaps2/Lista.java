package com.tjddm.eduardo.bilk.brasilmaps2;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Lista extends Fragment{
	private OnItemSelecionadoListaListener listener;
	ListView listView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
			savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_layout_lista, container, false);
		listView = (ListView) view.findViewById(R.id.listView1);
		listView.setOnItemClickListener(new OnItemClickListener() {
			
			public void onItemClick(AdapterView<?> parent, View v,
					int pos, long id) {
				
				String item = parent.getItemAtPosition(pos).toString();
				Log.d("ESCOLHA", "Item "+item+" escolhido.");
				listener.onItemSelected(item);
			}
		});
		return view;
	}
	public interface OnItemSelecionadoListaListener {
		public void onItemSelected(String item);
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnItemSelecionadoListaListener) {
			listener = (OnItemSelecionadoListaListener) activity;
		}
	}
	public void addAdapter(ArrayAdapter<String> adapter){
		listView.setAdapter(adapter);
	}
}
