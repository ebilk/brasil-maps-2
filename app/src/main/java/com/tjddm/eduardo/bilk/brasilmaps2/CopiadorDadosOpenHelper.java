package com.tjddm.eduardo.bilk.brasilmaps2;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;


public class CopiadorDadosOpenHelper extends SQLiteOpenHelper {
	
	private static String DB_PATH =	Environment.getDataDirectory()+"/data/com.tjddm.eduardo.bilk.brasilmaps2/databases/";
	private static String DB_NAME = "mapas_municipios.db";
	private static int DB_VERSION = 1;
	private Context context;

	public CopiadorDadosOpenHelper(Context c) {
		super(c, DB_NAME, null, DB_VERSION);
		context = c;
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
	
	public void criaBaseDados() throws IOException {
		boolean dbexist = verificaBaseDados();
		if (!dbexist) {
			this.getReadableDatabase();
			this.close();
			try {
				copiaBaseDados();
			} catch (IOException e) {
				throw new Error("Erro ao copiar DB:"+e);
			}
		}
	}
	private void copiaBaseDados() throws IOException {
		InputStream arqEntrada = context.getAssets().open(DB_NAME);
		String nomeArqSaida = DB_PATH + DB_NAME;
		OutputStream arqSaida = new FileOutputStream(nomeArqSaida);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = arqEntrada.read(buffer)) > 0) {
			arqSaida.write(buffer, 0, length);
		}
		arqSaida.flush();
		arqSaida.close();
		arqEntrada.close();
	}
	
	private boolean verificaBaseDados() {
		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

}
