package com.tjddm.eduardo.bilk.brasilmaps2;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tjddm.eduardo.bilk.brasilmaps2.Lista.OnItemSelecionadoListaListener;

public class MainActivity extends Activity implements OnItemSelecionadoListaListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Lista listaFrag = (Lista) getFragmentManager().findFragmentById(R.id.lista1);
		listaFrag.addAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,consultaBanco("SELECT nome FROM Estados ORDER BY nome")));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private ArrayList<String> consultaBanco(String query){
		CopiadorDadosOpenHelper copiador = new CopiadorDadosOpenHelper(this);

		try {
			copiador.criaBaseDados();
		} catch (IOException e) {
			e.printStackTrace();
		}

		SQLiteDatabase db = copiador.getReadableDatabase();

		ArrayList<String> array = new ArrayList<String>();

		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()){
			array.add(cursor.getString(0));
			while (cursor.moveToNext()){
				array.add(cursor.getString(0));
			}
		}
		db.close();
		return array;
	}

	@Override
	public void onItemSelected(String item) {

		MapFragment mapFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment2);

		ArrayList<String> cidades = consultaBanco("SELECT localidade FROM Estados e INNER JOIN Localidades l ON e.ibge=l.ibge_estado WHERE e.nome="+"'"+item+"'");
		ArrayList<String> lat =  consultaBanco("SELECT latitude FROM Estados e INNER JOIN Localidades l ON e.ibge=l.ibge_estado WHERE e.nome="+"'"+item+"'");
		ArrayList<String> lon = consultaBanco("SELECT longitude FROM Estados e INNER JOIN Localidades l ON e.ibge=l.ibge_estado WHERE e.nome="+"'"+item+"'");
		ArrayList<String> populacao = consultaBanco("SELECT populacao FROM Estados e INNER JOIN Localidades l ON e.ibge=l.ibge_estado WHERE e.nome="+"'"+item+"'");
		if (mapFrag != null && mapFrag.isInLayout()) {
			GoogleMap mapa = mapFrag.getMap();
			mapa.clear();
			for (int i=0;i<cidades.size();i++){
				MarkerOptions marker = new MarkerOptions()
				.position(new LatLng(Double.parseDouble(lat.get(i)), Double.parseDouble(lon.get(i))))
				.title(cidades.get(i).toUpperCase())
				.snippet("População: "+populacao.get(i)+" habitantes.");
				mapa.addMarker(marker);
			}
			MapaActivity.moveMapaCentroEstado(mapa,item, this);
		} else {
			Intent intent = new Intent(getApplicationContext(),MapaActivity.class);
			intent.putExtra(MapaActivity.ESTADO, item);
			intent.putStringArrayListExtra("cidades", cidades);
			intent.putStringArrayListExtra("lat", lat);
			intent.putStringArrayListExtra("lon", lon);
			intent.putStringArrayListExtra("populacao", populacao);
			startActivity(intent);
		}
				
	}

	
}
