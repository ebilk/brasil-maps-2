package com.tjddm.eduardo.bilk.brasilmaps2;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaActivity extends Activity {
	public static final String ESTADO = "Estado Padrão";
	ArrayList<String> cidades;
	ArrayList<String> latitude;
	ArrayList<String> longitude;
	ArrayList<String> populacao;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//se voltar para landscape finaliza a activity
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) 
		{
			finish();
			return;
		}
		setContentView(R.layout.activity_mapa);
		MapFragment mapaFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment2);
		GoogleMap map = mapaFrag.getMap();
		
		Bundle extras = getIntent().getExtras();
		if (extras!=null){
			String estado = extras.getString(ESTADO);
			cidades = extras.getStringArrayList("cidades");
			latitude = extras.getStringArrayList("lat");
			longitude = extras.getStringArrayList("lon");
			populacao = extras.getStringArrayList("populacao");
			map.clear();
			for (int i=0;i<cidades.size();i++){
				MarkerOptions marker = new MarkerOptions()
				.position(new LatLng(Double.parseDouble(latitude.get(i)), Double.parseDouble(longitude.get(i))))
				.title(cidades.get(i).toUpperCase())
				.snippet("População: "+populacao.get(i)+" habitantes.");
				map.addMarker(marker);
			}
			MapaActivity.moveMapaCentroEstado(map, estado, this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mapa, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static void moveMapaCentroEstado(GoogleMap mapa, String estado, Context contexto){
		System.out.println("Estado = "+estado);
		double latitude=0, longitude=0;
		int zoom=0;
        if (estado.equals("Acre")) {
            latitude = -9.1285244;
            longitude = -70.3077931;
            zoom = 8;

        } else if (estado.equals("Amazonas")) {
            latitude = -3.7857105;
            longitude = -64.9495526;
            zoom = 6;

        } else if (estado.equals("Roraima")) {
            latitude = 1.8456015;
            longitude = -61.8560613;
            zoom = 7;

        } else if (estado.equals("Amapá")) {
            latitude = 1.603494;
            longitude = -52.3764644;
            zoom = 7;

        } else if (estado.equals("Tocantins")) {
            latitude = -9.3180492;
            longitude = -48.219068;
            zoom = 7;

        } else if (estado.equals("Piauí")) {
            latitude = -6.8380377;
            longitude = -43.1823976;
            zoom = 6;

        } else if (estado.equals("Ceará")) {
            latitude = -5.321309;
            longitude = -39.3382674;
            zoom = 8;

        } else if (estado.equals("Rio Grande Do Norte")) {
            latitude = -5.9072667;
            longitude = -36.7754294;
            zoom = 9;

        } else if (estado.equals("Paraíba")) {
            latitude = -7.1644358;
            longitude = -36.7793751;
            zoom = 9;

        } else if (estado.equals("Alagoas")) {
            latitude = -9.658443;
            longitude = -36.6897704;
            zoom = 9;

        } else if (estado.equals("Sergipe")) {
            latitude = -10.5417791;
            longitude = -37.3227577;
            zoom = 9;

        } else if (estado.equals("Bahia")) {
            latitude = -13.4343695;
            longitude = -41.982751;
            zoom = 7;

        } else if (estado.equals("Minas Gerais")) {
            latitude = -18.5779703;
            longitude = -45.4514505;
            zoom = 7;

        } else if (estado.equals("Paraná")) {
            latitude = -24.6169813;
            longitude = -51.3214141;
            zoom = 8;

        } else if (estado.equals("Rio Grande Do Sul")) {
            latitude = -30.4163414;
            longitude = -53.6680033;
            zoom = 7;

        } else if (estado.equals("Mato Grosso Do Sul")) {
            latitude = -20.611376;
            longitude = -54.5449798;
            zoom = 7;

        } else if (estado.equals("Mato Grosso")) {
            latitude = -12.6955444;
            longitude = -55.9289973;
            zoom = 6;

        } else if (estado.equals("Goiás")) {
            latitude = -16.2898245;
            longitude = -48.7422954;
            zoom = 6;

        } else if (estado.equals("Distrito Federal")) {
            latitude = -15.7759668;
            longitude = -47.7976436;
            zoom = 11;

            latitude = -3.6250659;
            longitude = -52.4812983;
            zoom = 6;

        } else if (estado.equals("Pará")) {
            latitude = -3.6250659;
            longitude = -52.4812983;
            zoom = 6;

        } else if (estado.equals("Maranhão")) {
            latitude = -5.6558833;
            longitude = -45.2755116;
            zoom = 7;

        } else if (estado.equals("Rio de Janeiro")) {
            latitude = -22.9156912;
            longitude = -43.449703;
            zoom = 9;

        } else if (estado.equals("São Paulo")) {
            latitude = -23.262319;
            longitude = -45.8839345;
            zoom = 6;

        } else if (estado.equals("Santa Catarina")) {
            latitude = -27.6536999;
            longitude = -51.1508311;
            zoom = 8;

        } else if (estado.equals("Pernambuco")) {
            latitude = -8.3779283;
            longitude = -38.0825865;
            zoom = 8;

        } else if (estado.equals("Espírito Santo")) {
            latitude = -19.5968657;
            longitude = -40.7717683;
            zoom = 8;

        }

		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(new LatLng(latitude, longitude))
		.zoom(zoom)
		.build();

		Toast.makeText(contexto, "Estado de " + estado + " selecionado.",  Toast.LENGTH_SHORT).show();

		mapa.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

}
